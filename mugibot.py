import asyncio
import config
import discord
import datetime
import logging
import random
import requests
import traceback

from discord.ext import commands
from Helpers import *
#from Helpers.mugipixiv import *
from Helpers.mugidanbooru import *
#from pixivpy3 import *
from pybooru import Danbooru

#PIXIV_USER = config.PIXIV_CONFIG['pixiv_user']				# For logging in to Pixiv
#PIXIV_PASS = config.PIXIV_CONFIG['pixiv_pass']
BOT_PREFIX = config.APP_CONFIG['bot_prefix']				# Which prefix in chat we use
BOT_TOKEN = config.APP_CONFIG['bot_token']					# To log in to Discord
BOT_ID = config.APP_CONFIG['bot_id']						# To check which messages were posted by the bot (to edit them, etc)
DANBOORU_USER = config.DANBOORU_CONFIG['danbooru_user']		# For logging in to Danbooru
DANBOORU_KEY = config.DANBOORU_CONFIG['danbooru_key']

ALWAYS_SAFE_CHANNELS = config.APP_CONFIG['always_safe_channel_ids']
DEBUG_CHANNEL = config.APP_CONFIG['bot_debug_channel']

description = config.APP_CONFIG['bot_desc']

#pixivLogin = PixivAPI()
#pixivLogin.login(PIXIV_USER, PIXIV_PASS)
danbooruLogin = Danbooru('danbooru', username = DANBOORU_USER, api_key = DANBOORU_KEY)

bot = commands.Bot(command_prefix=BOT_PREFIX, description=description)
bot.remove_command('help')									# We're removing it here so we can add our own.
#mugipix = mugipixiv()
mugibooru = mugidanbooru()

########################## TEST STUFF ##########################

@bot.command()
async def test():
	await bot.send_message(bot.get_channel(DEBUG_CHANNEL), "Testing")
	#try:
	#	response = Helpers.anilistHelper.test()
	#	await bot.say(response)
	#except Exception as e:
	#	await bot.say(traceback.format_exc())

##########################	END TEST STUFF		##########################

##########################	BOT COMMANDS		##########################
##########################	PIXIV				##########################


#!!!!!!! PIXIV DISABLED BECAUSE USER BLOCKED OR SOMETHING !!!!!!!!!
# and frankly fuck fixing it


#@bot.group(name='Pixiv', description='Pixiv commands. Search Pixiv for a given alias. Ex: $p cute シアン(SHOWBYROCK!!)', brief='Pixiv commands', pass_context=True, aliases=["p", "pixiv"])
#async def pixiv(context):
#	"""Base Pixiv function that holds all Pixiv commands. If no valid subcommand is given, "cute" is tried by default."""
#	if context.invoked_subcommand is None:
#		searchString = context.message.content[3:]
#		await pixivFindImage(searchString)

#@pixiv.command(name='Set Alias', description='Set an alias for Pixiv', brief='Set an alias', aliases=["addalias", "setalias", "aa", "sa"])
#async def pixivSetAlias(keyword, tag):
#	"""Sets an alias for Pixiv. Aliases replace your keyword (what you type in chat) with the tag in the search, so you can use 'Cyan' instead of 'シアン(SHOWBYROCK!!)' which is a lot easier to type."""
#	await setAlias("pixiv", keyword, tag)

#@pixiv.command(name='Get Alias', description='Get an alias for Pixiv', brief='Get an alias', aliases=["getalias", "ga"])
#async def pixivGetAlias(keyword):
#	"""Gets an alias for Pixiv. 'all' for all aliases. Set one with pixivSetAlias."""
#	await getAlias("pixiv", keyword)

#@pixiv.command(name='Delete Alias', description='Delete an alias for Pixiv', brief='Delete an alias', aliases=["delalias", "da"])
#async def pixivRemAlias(keyword):
#	"""Removes an alias for Pixiv. Set one with pixivSetAlias."""
#	await remAlias("pixiv", keyword)

#@pixiv.command(name='Cute', description='Find a random image on Pixiv with this tag', brief='Find image', aliases=["c", "cute"])
#async def pixivCute(searchString = ''):
#	"""Finds a cute picture on Pixiv given a search string. Also checks aliases."""
#	await pixivFindImage(searchString)

#@pixiv.command(name='Lewd', description='Find a random lewd image on Pixiv', brief='Find a lewd', aliases=["l", "lewd"], pass_context=True)
#async def pixivLewd(context):
#	"""Finds a lewd picture on Pixiv. For some reason you can only search for one thing at a time with Pixiv so this is what we got."""
#	if context.message.channel.id in ALWAYS_SAFE_CHANNELS:
#		await bot.say("No lewd will be posted in this channel, as it is SFW only.")
#	else:
#		await bot.say(await pixivFindImage('R-18'))

#async def pixivFindImage(searchString = ''):
#	"""Finds a picture on Pixiv given a search string. Also checks tags."""
#	alias = await tagging.get_alias("pixiv", searchString)
#	if alias:
#		searchString = alias
#	await bot.say(await mugipix.find_image(pixivLogin, searchString))

##########################	END PIXIV			##########################

##########################	DANBOORU			##########################

@bot.group(name='Danbooru', description='Danbooru commands. Search Danbooru with a given tag. Ex: $d cute cyan', brief='Danbooru commands', pass_context=True, aliases=["d", "danbooru"])
async def danbooru(context):
	"""Base Danbooru function that holds all Danbooru commands. If no valid subcommand is given, "cute" is tried by default."""
	if context.invoked_subcommand is None:
		searchString = context.message.content[3:]
		await danbooruFindImage(searchString, context)

@danbooru.command(name='Set alias', description='Set an alias for Danbooru', brief='Set an alias', aliases=["addalias", "setalias", "aa", "sa"])
async def danbooruSetAlias(keyword, tag):
	"""Sets an alias for Danbooru. Aliases replace your keyword (what you type in chat) with the tag in the search, so you can use 'Cyan' instead of 'cyan_(show_by_rock!!)' which is a lot easier to type."""
	await setAlias("danbooru", keyword, tag)

@danbooru.command(name='Get alias', description='Get an alias for Danbooru', brief='Get an alias', aliases=["getalias", "ga"])
async def danbooruGetAlias(keyword):
	"""Gets an alias for Danbooru. 'all' for all aliases. Set one with danbooruSetAlias."""
	await getAlias("danbooru", keyword)

@danbooru.command(name='Del alias', description='Delete an alias for Danbooru', brief='Delete an alias', aliases=["delalias", "da"])
async def danbooruRemAlias(keyword):
	"""Removes an alias for Danbooru. Set one with danbooruSetAlias."""
	await remAlias("danbooru", keyword)

@danbooru.command(name='Get tag prefs', description='Get your tag preferences for Danbooru', brief='Get tag prefs', aliases=["gettagprefs", "gtp"], pass_context=True)
async def danbooruGetTagPrefs(context):
	await getTagPrefs("danbooru", context.message.author.id)

@danbooru.command(name='Set tag prefs', description='Set a new personal tag preferences for Danbooru', brief='Set tag prefs', aliases=["settagprefs", "stp"], pass_context=True)
async def danbooruSetTagPrefs(context, tag, wanted="yes"):
	wantedBool = parseWantedText(wanted)
	if wantedBool == None:
		await bot.say("I couldn't determine whether you wanted this tag or not. Try again. Valid options: 'y', 'yes', 'n', 'no'.")
		return

	await setTagPref("danbooru", context.message.author.id, tag, wantedBool)

@danbooru.command(name='Del tag pref', description='Delete a personal tag preference.', brief='Delete a personal tag pref', aliases=["dtp", "deltagpref"], pass_context=True)
async def danbooruDelTagPreference(context, tag):
	await delTagPref("danbooru", context.message.author.id, tag)

@danbooru.command(name='Cute', description='Find a random cute image on Danbooru', brief='Find a cute', aliases=["c", "cute"], pass_context=True)
async def danCute(context, searchQuery = ''):
	"""Finds a cute picture on Danbooru given a search string. Also checks aliases."""
	await danbooruFindImage(searchQuery, context, 'safe')

@danbooru.command(name='Lewd', description='Find a random lewd (rated questionable) image on Danbooru', brief='Find lewd', aliases=["l", "lewd"], pass_context=True)
async def danLewd(context, searchQuery = ''):
	"""Finds a lewd picture on Danbooru given a search string. Also checks aliases."""
	if context.message.channel.id in ALWAYS_SAFE_CHANNELS:
		await bot.say("No lewd will be posted in this channel, as it is SFW only.")
	else:
		await danbooruFindImage(searchQuery, context, 'questionable')

@danbooru.command(name='Very Lewd', description='Find a random very lewd (rated explicit) image on Danbooru', brief='Find porn', aliases=["vl", "verylewd"], pass_context=True)
async def danVeryLewd(context, searchQuery = ''):
	"""Finds a very lewd picture on Danbooru given a search string. Also checks aliases."""
	if context.message.channel.id in ALWAYS_SAFE_CHANNELS:
		await bot.say("No lewd will be posted in this channel, as it is SFW only.")
	else:
		await danbooruFindImage(searchQuery, context, 'explicit')

async def danbooruFindImage(searchQuery, context, rating = 'safe'):
	"""Finds a picture on Danbooru given a search string. Also checks tags."""
	alias = await tagging.get_alias("danbooru", searchQuery)
	tagPrefs = await personalTagging.listPersonalTagPreferences("danbooru", context.message.author.id)
	if alias:
		searchQuery = alias
	await bot.say(await mugibooru.find_image(danbooruLogin, searchQuery, rating = rating, personalTagPrefs = tagPrefs))

##########################	END DANBOORU		##########################

##########################	ANILIST				##########################

@bot.group(name='Anilist', description='Anilist commands.', brief='Anilist commands', pass_context=True, aliases=["a", "anilist"])
async def anilist(context):
	if context.invoked_subcommand is None:
		await bot.say("Invalid command.")

@anilist.command(name='Search anime', description='Look for an anime\'s ID so you can subscribe to it', brief='Find a series', aliases=["sa", "searchanime"])
async def anilistSearch(*, searchQuery: str):
	apiResult = Helpers.anilistHelper.searchAnime(searchQuery)
	await bot.say(apiResult)

@anilist.command(name='Next episode', description='Look for when the next anime episode airs', brief='See when the next episode airs', aliases=["ne", "nextepisode"])
async def anilistNextEpisode(animeId):
	apiResult = Helpers.anilistHelper.nextEpisodeAirsIn(animeId)
	await bot.say(apiResult)

@anilist.command(name='Subscribe', description='Subscribe to a series and get notified when the next episode airs', brief='Get notified when an episode airs', aliases=["sub", "subscribe"], pass_context=True)
async def anilistSubscribe(context, series_id, type = "USER"):
	if type.lower() == "user":
		apiResult = Helpers.anilistHelper.subscribeToSeries(context.message.author.id, type.upper(), series_id)
	elif type.lower() == "channel":
		apiResult = Helpers.anilistHelper.subscribeToSeries(context.message.channel.id, type.upper(), series_id)
	else:
		await bot.say("Invalid command. Syntax: $a sub [ID] [optional: CHANNEL]")
		return
	await bot.say(apiResult)

@anilist.command(name='Unsubscribe', description='Unsubscribe from a series and no longer get notified when the next episode airs', brief='Stop new episode notifications', aliases=["unsub", "unsubscribe"], pass_context=True)
async def anilistUnsubscribe(context, series_id, type = "USER"):
	if type.lower() == "user":
		apiResult = Helpers.anilistHelper.unsubscribeFromSeries(context.message.author.id, type.upper(), series_id)
	elif type.lower() == "channel":
		apiResult = Helpers.anilistHelper.unsubscribeFromSeries(context.message.channel.id, type.upper(), series_id)
	else:
		await bot.say("Invalid command. Syntax: $a unsub [ID] [optional: CHANNEL]")
		return
	await bot.say(apiResult)

@anilist.command(name='ListSubscriptions', description='Show the series you\'re subscribed to', brief='Show subscriptions', aliases=["listsubs", "listsubscriptions"], pass_context=True)
async def anilistListSubscriptions(context, type = "USER"):
	if type.lower() == "user":
		apiResult = Helpers.anilistHelper.listSubscriptions(context.message.author.id, type)
		target = context.message.author.name
	elif type.lower() == "channel":
		apiResult = Helpers.anilistHelper.listSubscriptions(context.message.channel.id, type.upper())
		target = context.message.channel.name
	else:
		await bot.say("Invalid command. Syntax: $a listsubs")
		return

	if apiResult is None:
		await bot.say("Couldn't find any subscriptions. If you're sure you should have some, make sure they haven't ended.")
		return

	msg = "Subscriptions for {0}:".format(target)
	msg += "```"
	msg += "[ID] - [NAME] - [NEXT AIR DATE (Amsterdam time)] - [EPISODE/TOTAL]\n"

	for row in apiResult:
		timeUntilAiring = row.nextepisode_airtime - datetime.datetime.now()

		dayDiff = row.nextepisode_airtime.date() - datetime.datetime.now().date()
		dayDiffString = ""
		if dayDiff.days >= 2:
			dayDiffString = "{0} days".format(dayDiff.days)
		elif dayDiff.days == 1:
			dayDiffString = "tomorrow, {0}h to go".format((timeUntilAiring.days * 24) + int(timeUntilAiring.seconds / 3600))
		elif dayDiff.days == 0:
			dayDiffString = "today, {0}h to go".format(int(timeUntilAiring.seconds / 3600))
		else:
			"some time ago"

		msg += "{0} - {1} - {2} ({5}) - {3}/{4}\n".format(
			row.series_id												# 0
			, row.series_name											# 1
			, row.nextepisode_airtime.strftime('%a %d %b @ %H:%M')		# 2
			, row.nextepisode_number									# 3
			, row.series_totalepisodes									# 4
			, dayDiffString)											# 5
	msg += "```"
	await bot.say(msg)

##########################	END ANILIST			##########################

##########################	PERSONAL			##########################

@bot.group(name='Me', description='Commands for you only. Personal tag preferences and soon™ watchlists for anime/manga.', brief='Commands for personal use', pass_context=True, aliases=["me"])
async def personal(context):
	if context.invoked_subcommand is None:
		await bot.say("Invalid command. Try '$help me' for more info.")

@personal.command(name='Set Tag Preference', description='Set a personal tag preference. Usage: $me apt/stp [danbooru/pixiv] [tag] [yes/no] Example: $me atp danbooru bikini yes', brief='Add a personal tag pref', pass_context=True, aliases=["atp", "stp", "settagpreference", "addtagpreference"])
async def personalSetTagPreference(context, module, tag, wanted):
	wantedBool = parseWantedText(wanted)
	if wantedBool == None:
		await bot.say("I couldn't determine whether you wanted this tag or not. Try again. Valid options: 'y', 'yes', 'n', 'no'.")
		return
	
	if not isModuleKnown(module):
		await bot.say("I couldn't figure out which module you meant. Try again. Valid options: 'pixiv', 'danbooru'")
		return

	await setTagPref(module, context.message.author.id, tag, wantedBool)

@personal.command(name='Get Tag Preferences', description='Get personal tag preferences. Usage: $me gtp [danbooru/pixiv] Example: $me gtp danbooru', brief='View personal tags prefs', pass_context=True, aliases=["gtp", "gettagpreferences"])
async def personalGetTagPreferences(context, module):
	if not isModuleKnown(module):
		await bot.say("I couldn't figure out which module you meant. Try again. Valid options: 'pixiv', 'danbooru'")
		return

	await getTagPrefs(module, context.message.author.id)

@personal.command(name='Del Tag Preference', description='Delete a personal tag preference. Usage: $me dtp [danbooru/pixiv] [tag] Example: $me dtp danbooru bikini', brief='Delete a personal tag pref', pass_context=True, aliases=["dtp", "deletetagpreference"])
async def personalDelTagPreference(context, module, tag):
	if not isModuleKnown(module):
		await bot.say("I couldn't figure out which module you meant. Try again. Valid options: 'pixiv', 'danbooru'")
		return

	await delTagPref(module, context.message.author.id, tag)

def isModuleKnown(module):
	if module in ['pixiv', 'danbooru']:
		return True
	return False

def parseWantedText(wanted):
	if wanted == "yes" or wanted == "y":
		return True
	elif wanted == "no" or wanted == "n":
		return False
	return

##########################	END PERSONAL		##########################

##########################	SELF COMMANDS		##########################

@bot.command(name='Delete', description='Delete Mugibot\'s last message to this channel.', brief='Remove Mugi\'s last message', aliases=["delete"], pass_context=True)
async def delMsg(context):
	"""Removes the bot's last message by editing it to something neutral. Useful in case the bot inadvertedly posts something inappropriate."""
	mgs = []
	searchLimit = 10
	channel = context.message.channel
	messages = ["Removing...", "Never existed", "Yes, commander...", "Without a trace", "Deconstructing..."]

	logs = bot.logs_from((channel), limit = searchLimit)
	messageFound = False			# Necessary since 1. can't manually make a new Message() object and 2. toEditMessage is assigned in the for loop.
									# If it can't find any appropriate messages, it's empty, and we can't check against an unassigned variable.

	async for x in logs:
		if x.author.id == BOT_ID:
			toEditMessage = x
			messageFound = True
			break

	if messageFound:
		await bot.edit_message(toEditMessage, random.choice(messages))
	else:
		await bot.say("Could not find one of my messages to remove. Searched the last {0} messages.".format(str(searchLimit)))
	
@bot.command(name='help', description='Show help for Mugibot')
async def help():
	msg = '''Commands for Mugibot:
	```$help: Shows this message.
$delete: Delete the previous message.

$me: Personal commands. Subcommands:
	1. settagpreference/stp [danbooru/pixiv] [tag] [yes/no] - Set tag preference, make tags more or less likely to appear.
	2. gettagpreferences/gtp [danbooru/pixiv] - Get personal tag preferences.
	3. deletetagpreference/dtp [danbooru/pixiv] [tag] - Delete tag preference.

$a: Anilist commands. Subcommands:
	1. searchanime/sa [search term] - Search anime for its ID. Use this ID for the other commands.
	2. subscribe/sub [id] - Subscribe to a series so you get updates about it via PM.
	3. unsubscribe/unsub [id] - Unsubscribe from the series.
	4. listsubs - Show what you're subscribed to.
	5. nextepisode/ne [id] - When does the next episode of this series air?

$d: Danbooru commands. No subcommand is equivalent to cute search. Subcommands:
	1. cute/c [tag or alias] - Cute search for tag or alias (if available).
	2. lewd/l [tag or alias] - Lewd search.
	3. verylewd/vl [tag or alias] - Very lewd search.

	4. setalias/sa [alias] [tag] - Set alias, makes searching easier. Alias is what you want to use in chat, tag is what the tag is on danbooru.
	5. getalias/ga [alias] - Get alias, 'all' as an alias for all of them.
	6. delalias/da [alias] - Delete alias.
	7. settagpreference/stp [tag] [yes/no] - Set tag preference so it gets higher or lower priority.
	8. deletetagpreference/dtp [tag] - Delete tag preference.
	9. gettagpreferences/gtp - Get tag preferences.

$p: Pixiv commands. No subcommand is equivalent to cute search. Subcommands:
	1. cute/c [tag or alias] - Cute search
	2. lewd/l [tag or alias] - Lewd search

	3. setalias/sa [alias] [tag] - Set alias, makes searching easier. Alias is what you want to use in chat, tag is what the tag is on pixiv.
	4. getalias/ga [alias] - Get alias, 'all' as an alias for all of them.
	5. delalias/da [alias] - Delete alias```'''
	await bot.say(msg)

##########################	END SELF COMMANDS	##########################
##########################	END BOT COMMANDS	##########################

##########################	PRIVATE FUNCTIONS	##########################

async def getAlias(module, keyword):
	"""Finds an alias based on the module (Pixiv, Danbooru...)."""
	if keyword != "all":
		response = await tagging.get_alias(module, keyword)
		if response:
			await bot.say("'{0}' => '{1}'".format(keyword, response))
		else:
			await bot.say("Tag '{0}' was not found in module '{1}'.".format(keyword, module))
	else:
		result = await tagging.get_aliases(module)
		msg = "All '{0}' aliases:\n```".format(module)
		for alias in result:
			msg += "'{0}' => '{1}'\n".format(alias.keyword, alias.tag)
		msg += "```"
		await bot.say(msg)

async def setAlias(module, keyword, tag):
	"""Sets an alias based on the module (Pixiv, Danbooru...)."""
	response = await tagging.set_alias(module, keyword, tag)
	if response:
		await bot.say("Alias '{0}' successfully added (or updated) as '{1}' in module '{2}'".format(keyword, tag, module))
	else:
		await bot.say("Alias '{0}' was not added for some reason, poke NearNile.".format(keyword))

async def remAlias(module, keyword):
	"""Removes an alias based on the module (Pixiv, Danbooru...)."""
	response = await tagging.rem_tag(module, keyword)
	if response:
		await bot.say("Alias '{0}' successfully removed from module '{1}'.".format(keyword, module))
	else:
		await bot.say("Alias '{0}' was not removed; it could be that it never existed.".format(keyword))

async def getTagPrefs(module, userId):
	response = await personalTagging.listPersonalTagPreferences(module, userId)
	if response and len(response) > 0:
		msg = "All '{0}' tag preferences:\n```".format(module)
		msg += "'Tag': Wanted (Yes/No)\n"
		for tagPref in response:
			wantedString = "No"
			if tagPref.wanted:
				wantedString = "Yes"
			msg += "'{0}': {1}\n".format(tagPref.tag, wantedString)
		msg += "```"
		await bot.say(msg)
	else:
		await bot.say("No personal tag preferences found for you in module '{0}'".format(module))

async def setTagPref(module, userId, tag, wanted):
	response = await personalTagging.setPersonalTagPreference(module, userId, tag, wanted)
	if response:
		wantedString = "No"
		if wanted:
			wantedString = "Yes"
		await bot.say("Personal preference for tag '{0}' in module '{1}' set or updated to '{2}'.".format(tag, module, wantedString))
	else:
		await bot.say("Personal preference for tag '{0}' in module '{1}' could not be set for some reason, poke NearNile.".format(tag, module))

async def delTagPref(module, userId, tag):
	response = await personalTagging.delPersonalTagPreference(module, userId, tag)
	if response:
		await bot.say("Personal preference for tag '{0}' in module '{1}' removed.".format(tag, module))
	else:
		await bot.say("Personal preference for tag '{0}' in module '{1}' could not be removed for some reason, poke NearNile.".format(tag, module))

##########################	END PRIVATE FUNCTS	##########################

##########################	PERIODIC FUNCTIONS	##########################
async def set_pixiv_login():
	"""Refreshes the Pixiv login as it expires every hour otherwise."""
	while not bot.is_closed:
		await asyncio.sleep(3600)
		print('refresh token')
		pixivLogin.auth(refresh_token=pixivLogin.refresh_token)

async def check_series():
	while not bot.is_closed:
		# Check subs
		anilistResult = Helpers.anilistHelper.checkSubscriptions()
		sleepTime = 120
		numMessagesSent = 0

		for row in anilistResult:
			# For each result, figure out the channel destination...
			if row.subscriber_type.lower() == "user":
				destination = await bot.get_user_info(row.subscriber_id)
			elif row.subscriber_type.lower() == "channel":
				destination = bot.get_channel(row.subscriber_id)
			# And send the message
			await bot.send_message(destination, "Episode {1}/{2} of {0} has aired! Go watch it!".format(row.series_name, row.nextepisode_number, row.series_totalepisodes))
			# Sleep to get around throttling somewhat
			await asyncio.sleep(1)
			# Adjust sleep time to pretend to keep in sync with the schedule
			if sleepTime >= 10:
				sleepTime -= 1
			numMessagesSent += 1

		if numMessagesSent > 0:
			await bot.send_message(bot.get_channel(DEBUG_CHANNEL), "Sent {0} series new episode messages.".format(numMessagesSent))
		await asyncio.sleep(sleepTime)

async def update_next_airing():
	while not bot.is_closed:
		# Update series that need updating (have episodes left but no next episode time) every hour
		waitTime = 3600
		numSeriesUpdated = Helpers.anilistHelper.updateNextAiring()
		if numSeriesUpdated > 0:
			await bot.send_message(bot.get_channel(DEBUG_CHANNEL), "Tried updating {0} series with new info.".format(numSeriesUpdated))
		await asyncio.sleep(waitTime)

##########################	END PERIODIC FUNCTIONS	#######################

@bot.event
async def on_ready():
	print('Logged in as')
	print(bot.user.name)
	print(bot.user.id)
	print(dbHelper.init_db())
	bot.loop.create_task(check_series())
	bot.loop.create_task(update_next_airing())
	print('------')

bot.loop.create_task(set_pixiv_login())

while True:
	bot.run(BOT_TOKEN)
	# If this ever completes, this typically means the connection was lost. Try again in 5 minutes.
	asyncio.sleep(300)