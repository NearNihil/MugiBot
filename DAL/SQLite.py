import os
from peewee import *
from Models import *
from datetime import datetime
from datetime import timedelta
import DAL.anilist_api

db = SqliteDatabase(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'mugibot.db')))

def init():
	db.connect()
	db.create_tables([
		alias.alias
		,personalTagPreference.personalTagPreference
		,seriesSubscription.seriesSubscription
		,seriesAirDate.seriesAirDate
	])
	db.close()
	return 'DB initialized'

##########################	ALIASES					##########################

def listAliasesForModule(module):
	db.connect()
	try:
		get = alias.alias.select().where(alias.alias.module == module)
		db.close()
		return list(get)
	except DoesNotExist:
		db.close()
		return
	return

def setAlias(module, keyword, tag):
	get = __getAlias__(module, keyword)
	db.connect()
	if get:
		get.tag = tag
		get.save()
	else:
		set = alias.alias(module = module, keyword = keyword, tag = tag)
		set.save()
	db.close()
	return True

def getAlias(module, keyword):
	get = __getAlias__(module, keyword)
	if get:
		return get.tag
	else:
		return

def delAlias(module, keyword):
	alias = __getAlias__(module, keyword)
	alias.delete_instance()
	return True

def __getAlias__(module, keyword):
	db.connect()
	# Piece of shit throws an exception when there's no rows returned
	try:
		get = alias.alias.get(alias.alias.module == module, alias.alias.keyword == keyword)
		db.close()
		return get
	except DoesNotExist:
		db.close()
		return
	return

##########################	END ALIASES				##########################

##########################	PERSONAL TAG PREFS		##########################

def setPersonalTagForModule(module, userId, tag, wanted):
	get = __getPersonalTagPreference__(module, userId, tag)
	db.connect()
	if get:
		get.wanted = wanted
		get.save()
	else:
		set = personalTagPreference.personalTagPreference(module = module, userId = userId, tag = tag, wanted = wanted)
		set.save()
	db.close()
	return True

def listPersonalTagsForModule(module, userId):
	db.connect()
	try:
		get = personalTagPreference.personalTagPreference.select().where(personalTagPreference.personalTagPreference.module == module, personalTagPreference.personalTagPreference.userId == userId)
		db.close()
		return list(get)
	except DoesNotExist:
		db.close()
		return
	return

def getPersonalTagPreference(module, userId, tag):
	get = __getPersonalTagPreference__(module, userId, tag)
	if get:
		return get.wanted
	else:
		return

def delPersonalTagPreference(module, userId, tag):
	alias = __getPersonalTagPreference__(module, userId, tag)
	alias.delete_instance()
	return True

def __getPersonalTagPreference__(module, userId, tag):
	db.connect()
	# Piece of shit throws an exception when there's no rows returned
	try:
		get = personalTagPreference.personalTagPreference.get(personalTagPreference.personalTagPreference.module == module,
																personalTagPreference.personalTagPreference.userId == userId,
																personalTagPreference.personalTagPreference.tag == tag)
		db.close()
		return get
	except DoesNotExist:
		db.close()
		return
	return

##########################	END PERSONAL TAG PREFS	##########################
##########################	SUBSCRIPTIONS			##########################

# series_provider and series_type hardcoded until needed
def subscribeToSeries(subscriber_id, subscriber_type, series_id, series_provider = "ANILIST", series_type = "ANIME"):
	subscrObj = seriesSubscription.seriesSubscription
	seriesObj = seriesAirDate.seriesAirDate
	db.connect()
	try:
		# See if series exists. Will throw exception if it doesn't
		getSeriesList = seriesObj.select().where(seriesObj.series_id == series_id, seriesObj.series_provider == series_provider, seriesObj.series_type == series_type).limit(1)
		if len(getSeriesList) > 0:
			getSeries = getSeriesList[0]
		
		#if getSeries.id is not None:
			# If it exists, might as well update it
			__updateSeries__(getSeries.series_id)

			# If the series exists but has no next episode, don't subscribe
			if getSeries.nextepisode_airtime is None:
				return None
					   
			try:
				# See if subscriber already exists, if they do, no action necessary
				getSubscr = subscrObj.get(
						subscrObj.subscriber_id == subscriber_id,
						subscrObj.subscriber_type == subscriber_type,
						subscrObj.seriesairdate_id == getSeries.id
					)
			except DoesNotExist:
				# If subscriber doesn't exist, make them
				setSubscr = subscrObj(
					subscriber_id = subscriber_id,
					subscriber_type = subscriber_type,
					seriesairdate_id = getSeries.id
				)
				setSubscr.save()
			db.close()
			return getSeries.series_name

		else:
			# Break up Anilist API call into more digestible bits
			anilistResult = DAL.anilist_api.getAnimeDetailsForNextEpisode(series_id)
			anilistData = anilistResult['data']['Media']
			_series_name = anilistData['title']['romaji']
			_series_totalepisodes = anilistData['episodes']
			try:
				_nextepisode_timeuntil = anilistData['nextAiringEpisode']['timeUntilAiring']
			except TypeError:
				db.close()
				return None
			_nextepisode_airtime = datetime.now() + timedelta(seconds = _nextepisode_timeuntil)
			_nextepisode_number = anilistData['nextAiringEpisode']['episode']

			# 20180923: Total Episodes CAN be empty apparently
			if _series_totalepisodes is None:
				_series_totalepisodes = 10000

			# If series doesn't exist, the subscriber doesn't either, so make them both
			setSeries = seriesObj(
					series_id = series_id,
					series_name = _series_name,
					series_provider = series_provider,
					series_type = series_type,
					nextepisode_airtime = _nextepisode_airtime,
					nextepisode_number = _nextepisode_number,
					series_totalepisodes = _series_totalepisodes
				)
			setSeries.save()
		
			setSubscr = subscrObj(
					subscriber_id = subscriber_id,
					subscriber_type = subscriber_type,
					seriesairdate_id = setSeries.id
				)
			setSubscr.save()
			db.close()
			return setSeries.series_name
		return False
	except:
		db.close()
		return False

def listSubscriptions(subscriber_id, subscriber_type):
	subscrObj = seriesSubscription.seriesSubscription
	seriesObj = seriesAirDate.seriesAirDate
	result = []

	db.connect()
	getSubscr = subscrObj.select().where(
		subscrObj.subscriber_id == subscriber_id,
		subscrObj.subscriber_type == subscriber_type,
	)

	for subscription in getSubscr:
		try:
			series = seriesObj.get(seriesObj.id == subscription.seriesairdate_id)
			if series.nextepisode_airtime:
				result.append(series)
		except DoesNotExist:
			continue
	db.close()
	return result

def unsubscribeFromSeries(subscriber_id, subscriber_type, series_id):
	subscrObj = seriesSubscription.seriesSubscription
	try:
		seriesObj_id = __getSeriesBySeriesId__(series_id)
		db.connect()
		getSubscr = subscrObj.get(
				subscrObj.subscriber_id == subscriber_id,
				subscrObj.subscriber_type == subscriber_type,
				subscrObj.seriesairdate_id == seriesObj_id
			)
		getSubscr.delete_instance()
		db.close()
		return True
	except DoesNotExist:
		db.close()
		return True
	return False

def checkSubscriptions():
	# References so it's easier to read the code
	subscrObj = seriesSubscription.seriesSubscription
	seriesObj = seriesAirDate.seriesAirDate
	resultList = []

	# Select all series and check if we have subs TODO: prune unsubbed series?
	seriesList = seriesObj.select()
	for series in seriesList:
		# If the series has aired, do stuff
		if series.nextepisode_airtime is not None:
			seriesHasAired = datetime.now() > series.nextepisode_airtime
		else:
			continue
		seriesHasEpisodesLeft = series.nextepisode_airtime is not None
		if seriesHasAired and seriesHasEpisodesLeft:
			# Find subs so we can notify them
			subscribers = subscrObj.select().where(subscrObj.seriesairdate_id == series.id)
			for sub in subscribers:
				resultList.append(Models.subscriptionDbResult.subscriptionDbResult(
					sub.subscriber_id
					, series.series_name
					, series.nextepisode_number
					, series.series_totalepisodes
					, sub.subscriber_type))
			__updateSeries__(series.series_id)
	return resultList

def UpdateNextAiring():
	seriesObj = seriesAirDate.seriesAirDate
	numSeriesUpdated = 0
	try:
		db.connect()
		getSeries = seriesObj.select().where(seriesObj.nextepisode_airtime is None, seriesObj.nextepisode_number < seriesObj.series_totalepisodes)
		db.close()
		try:
			for series in getSeries:
				__updateSeries__(series.series_id)
				numSeriesUpdated += 1
			return numSeriesUpdated
		except:
			return
	except:
		db.close()
	return numSeriesUpdated

def __getSeriesByPK__(id):
	seriesObj = seriesAirDate.seriesAirDate
	db.connect()
	try:
		getSeries = seriesObj.get(seriesObj.id == id)
		db.close()
		return getSeries
	except DoesNotExist:
		db.close()
		return False

def __getSeriesBySeriesId__(series_id, series_provider = "ANILIST", series_type = "ANIME"):
	seriesObj = seriesAirDate.seriesAirDate
	db.connect()
	try:
		getSeries = seriesObj.get(seriesObj.series_id == series_id, seriesObj.series_provider == series_provider, seriesObj.series_type == series_type)
		db.close()
		return getSeries
	except DoesNotExist:
		db.close()
		return False

def __updateSeries__(series_id):
	seriesObj = seriesAirDate.seriesAirDate
	try:
		# Get the series
		getSeries = seriesObj.get(seriesObj.series_id == series_id, seriesObj.series_provider == "ANILIST", seriesObj.series_type == "ANIME")
		anilistResult = DAL.anilist_api.getAnimeDetailsForNextEpisode(series_id)

		# Assign new values
		anilistData = anilistResult['data']['Media']
		_series_totalepisodes = anilistData['episodes']

		try:
			if _series_totalepisodes is not None:
				getSeries.series_totalepisodes = _series_totalepisodes
			_nextepisode_timeuntil = anilistData['nextAiringEpisode']['timeUntilAiring']
			getSeries.nextepisode_airtime = datetime.now() + timedelta(seconds = _nextepisode_timeuntil)
			getSeries.nextepisode_number = anilistData['nextAiringEpisode']['episode']
		except:
			getSeries.nextepisode_airtime = None

		# And save
		getSeries.save()
		return True
	except DoesNotExist:
		# If it doesn't exist, well at least don't crash
		return False
	return

##########################	END SUBSCRIPTIONS		##########################