import requests
import jsonpickle

url = 'https://graphql.anilist.co'

def test():
	query = '''
	query ($searchQuery: String) {
		Page (page:1, perPage:10) {
			pageInfo{
			  total
			}
			media(search: $searchQuery, type: ANIME) {
				id
				title {
				  romaji
				}
				nextAiringEpisode {
					timeUntilAiring
				}
			}
		}
	}
	'''

	variables = {
		'searchQuery': "MaiDragon"
	}

	response = requests.post(url, json={'query': query, 'variables': variables}).content
	return response

def getNextAiringEpisode(animeId):
	#https://anilist.co/graphiql?query=%7B%0A%20%20Media(id%3A%20101474%2C%20type%3A%20ANIME)%20%7B%0A%20%20%20%20title%20%7B%0A%20%20%20%20%20%20romaji%0A%20%20%20%20%7D%0A%20%20%20%20nextAiringEpisode%20%7B%0A%20%20%20%20%20%20timeUntilAiring%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A
	query = '''
	query ($id: Int)
	{
	  Media(id: $id, type: ANIME) {
		title {
		  romaji
		}
		nextAiringEpisode {
			timeUntilAiring
		}
	  }
	}
	'''

	variables = {
		'id': animeId
	}

	url = 'https://graphql.anilist.co'

	response = requests.post(url, json={'query': query, 'variables': variables}).content.decode('utf-8')
	return __parseResponse__(response)

def searchAnime(searchQuery):
	query = '''
	query ($searchQuery: String) {
		Page (page:1, perPage:10) {
			pageInfo{
			  total
			}
			media(search: $searchQuery, type: ANIME) {
				id
				title {
				  romaji
				}
			}
		}
	}
	'''

	variables = {
		'searchQuery': searchQuery
	}

	response = requests.post(url, json={'query': query, 'variables': variables}).content.decode('utf-8')
	return __parseResponse__(response)

def getAnimeDetailsForNextEpisode(animeId):
	#https://anilist.co/graphiql?query=%7B%0A%20%20Media(id%3A%20101474%2C%20type%3A%20ANIME)%20%7B%0A%20%20%20%20title%20%7B%0A%20%20%20%20%20%20romaji%0A%20%20%20%20%7D%0A%20%20%20%20nextAiringEpisode%20%7B%0A%20%20%20%20%20%20timeUntilAiring%0A%20%20%20%20%7D%0A%20%20%7D%0A%7D%0A
	query = '''
	query ($id: Int)
	{
	  Media(id: $id, type: ANIME) {
		title {
		  romaji
		}
		episodes
		nextAiringEpisode {
			timeUntilAiring
			episode
		}
	  }
	}
	'''

	variables = {
		'id': animeId
	}

	url = 'https://graphql.anilist.co'

	response = requests.post(url, json={'query': query, 'variables': variables}).content.decode('utf-8')
	return __parseResponse__(response)

def __parseResponse__(response):
	try:
		retVal = jsonpickle.loads(response)
	except:
		retVal = jsonpickle.load(response)
	return retVal