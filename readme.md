# Mugibot

Mugibot is a Discord bot for posting anime.

## Development
For security reasons, config.py is not included, but should look like this:
```
APP_CONFIG = {
	'bot_prefix' : "$",
	'bot_id' : "BOT_ID",
	'bot_token' : "TOKEN",
	'bot_desc' : '''Mugi, ready to serve tea and anime.''',
	'always_safe_channel_ids' : [
		"1337"
		,"80085"
	],
	'bot_debug_channel' : "527948597610938383"
}

DANBOORU_CONFIG = {
	'danbooru_user' : "DANBOORU",
	'danbooru_key' : "KEY",
	'globalDesirableTags': ["cat_ears", "animal_ears", "smile", "maid", "thighhighs"],
	'globalUndesirableTags': ["pee", "peeing", "blood", "guro", "scat", "furry", "yaoi", "male_focus"],
	'safeWantedTags': ["hug"],
	'safeUnwantedTags': ["panties", "cameltoe", "ass", "pornography", "underwear", "bra", "sideboob", "bottomless", "topless"],
	'questionableWantedTags': ["panties", "cameltoe", "ass", "underwear", "bikini", "bra", "sideboob", "bottomless", "topless"],
	'questionableUnwantedTags': [],
	'explicitWantedTags': ["naked_apron", "nipples", "pussy"],
	'explicitUnwantedTags': []
}

PIXIV_CONFIG = {
	'pixiv_user' : "EMAIL@PROVIDER.TLD",
	'pixiv_pass' : "PASSWORD"
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
