import DAL.SQLite

modules = ["pixiv", "danbooru"]

async def setPersonalTagPreference(module, userId, tag, wanted):
	if __checkModule__(module):
		return DAL.SQLite.setPersonalTagForModule(module, userId, tag, wanted)
	return

async def listPersonalTagPreferences(module, userId):
	if __checkModule__(module):
		return DAL.SQLite.listPersonalTagsForModule(module, userId)
	return []

async def getPersonalTagPreference(module, userId, tag):
	if __checkModule__(module):
		return DAL.SQLite.getPersonalTagPreference(module, userId, tag)
	return

async def delPersonalTagPreference(module, userId, tag):
	if __checkModule__(module):
		return DAL.SQLite.delPersonalTagPreference(module, userId, tag)
	return

def __checkModule__(module):
	if module in modules:
		return True
	return False