import DAL.SQLite

modules = ["pixiv", "danbooru"]

async def get_aliases(module):
	"""Get all tags for a module."""
	if __checkModule__(module):
		return DAL.SQLite.listAliasesForModule(module)
	return

async def get_alias(module, keyword):
	"""Find an alias for a given module.
	
	Returns:
		The tag the alias stands for if it exists;
		Nothing otherwise."""
	if __checkModule__(module):
		return DAL.SQLite.getAlias(module, keyword.lower())
	return

async def set_alias(module, keyword, tag):
	"""Sets an alias for a tag and a given module.
	
	Returns:
		True if the operation succeeds;
		False otherwise."""
	if __checkModule__(module):
		return DAL.SQLite.setAlias(module, keyword.lower(), tag)
	return

async def rem_tag(module, keyword):
	"""Removes an alias for a given module.
	
	Returns:
		True if the operation succeeds;
		False otherwise."""
	if __checkModule__(module):
		return DAL.SQLite.delAlias(module, keyword.lower())
	return

def __checkModule__(module):
	if module in modules:
		return True
	return False