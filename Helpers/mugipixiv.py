import config
import random
import requests

from pixivpy3 import *

class mugipixiv():
	def hello(self):
		return 'MugiPixiv.hello() called (this is a test)'

	async def find_image(self, pixivLogin, searchQuery):
		"""Finds an image on Pixiv given the login data and the search query.
		
		Returns:
		A pixiv illustration URL if there were results;
		A message saying 'No search results' if there were no results."""
		pixivPublicApi = pixivLogin
		json_result = pixivPublicApi.search_works(searchQuery)
		if json_result:
			numPages = json_result.pagination.pages
			randomInt = random.randint(1, numPages)
			
			images = pixivPublicApi.search_works(searchQuery, randomInt).response
			picked = random.choice(images)
			
			response = "https://www.pixiv.net/member_illust.php?mode=medium&illust_id=" + str(picked.id)
			
			return(response)
		else:
			return 'No search results for "{}".'.format(searchQuery)