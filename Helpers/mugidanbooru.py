import config
import random

from Helpers import tagging
from Models import danbooruItem
from pybooru import Danbooru

class mugidanbooru():
	def hello(self):
		return 'MugiDanbooru.hello() called (this is a test)'

	async def find_image(self, danbooruLogin, searchQuery, rating = "safe", personalTagPrefs = []):
		"""Finds an image using the Pybooru API wrapper, based on the search query, tags, and rating.
		
		Returns:
			A string containing a URL to a Danbooru post if results were found
			A string saying 'no search results for searchQuery' otherwise."""
		actualQuery = searchQuery + " rating:{0}".format(rating)
		localWantedTags = []
		localUnwantedTags = []
		wantedTags = config.DANBOORU_CONFIG['globalDesirableTags']
		unwantedTags = config.DANBOORU_CONFIG['globalUndesirableTags']

		if rating == "safe":
			localWantedTags = config.DANBOORU_CONFIG['safeWantedTags']
			localUnwantedTags = config.DANBOORU_CONFIG['safeUnwantedTags']
		elif rating == "questionable":
			localWantedTags = config.DANBOORU_CONFIG['questionableWantedTags']
			localUnwantedTags = config.DANBOORU_CONFIG['questionableUnwantedTags']
		elif rating == "explicit":
			localWantedTags = config.DANBOORU_CONFIG['explicitWantedTags']
			localUnwantedTags = config.DANBOORU_CONFIG['explicitUnwantedTags']

		# Fix tags for rating-dependent likes/dislikes
		for tag in localWantedTags:
			if tag not in wantedTags:
				wantedTags.append(tag)
			if tag in unwantedTags:
				unwantedTags.remove(tag)
		for tag in localUnwantedTags:
			if tag in wantedTags:
				wantedTags.remove(tag)
			elif tag not in unwantedTags:
				unwantedTags.append(tag)

		# Fix tags based on personal preferences
		for pref in personalTagPrefs:
			# Tag is wanted but not in the list yet
			if pref.tag not in wantedTags and pref.wanted:
				wantedTags.append(pref.tag)
			# Tag is not wanted but in the wanted list
			elif pref.tag in wantedTags and not pref.wanted:
				wantedTags.remove(pref.tag)
			# Tag is not unwanted but not in the list yet
			elif pref.tag not in unwantedTags and not pref.wanted:
				unwantedTags.append(pref.tag)
			# Tag is wanted but in the unwanted list
			elif pref.tag in unwantedTags and pref.wanted:
				unwantedTags.remove(pref.tag)

		json_result = self.search_danbooru(danbooruLogin, actualQuery, wantedTags = wantedTags, unwantedTags = unwantedTags)
		if json_result:
			return json_result
		else:
			return 'No search results for "{0}".'.format(actualQuery)

	def search_danbooru(self, danbooruLogin, searchQuery, wantedTags = [], unwantedTags = []):
		"""The actual searching function.

		Parameters:
			danbooruLogin: The logged-in Danbooru object to authenticate with.
			searchQuery: What to search for.
			wantedTags (optional): Desirable tags to grant more points if they're present in a post.
			unwantedTags (optional): Undesirable tags to deduct points from a post if they're present.
		
		Returns:
			A string containing the 'best' or 'most suitable' URL to a post on Danbooru, if results were found
			None otherwise."""
		json_result = danbooruLogin.post_list(tags = searchQuery, random = True, limit = 10)
		if json_result:
			results = []
			for item in json_result:
				# https://stackoverflow.com/questions/3182183/creating-a-list-of-objects-in-python
				results.append(danbooruItem.danbooruItem(item['id'], item['score'], item['fav_count'], item['is_deleted'], item['tag_string'], wantedTags = wantedTags, unwantedTags = unwantedTags))

			# https://stackoverflow.com/questions/403421/how-to-sort-a-list-of-objects-based-on-an-attribute-of-the-objects
			results.sort(key = lambda x: x.overallScore, reverse=True)
			winner = results[0].url
			return winner
		else:
			return None