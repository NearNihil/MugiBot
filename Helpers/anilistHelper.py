import DAL.anilist_api
import DAL.SQLite
import json

def test():
	return subscribeToSeries(169870581506899970, 'USER', 103209)
	#anilistApiResponse = DAL.anilist_api.test().decode('utf-8')
	#try:
	#	apiResult = json.loads(anilistApiResponse)
	#except:
	#	apiResult = json.load(anilistApiResponse)
	#if not __apiResultIsOk__(apiResult):
	#	return "Invalid JSON; poke NearNile"
	#data = apiResult['data']['Page']
	#totalEntries = data['pageInfo']['total']
	#if totalEntries == 0:
	#	return "No anime found for '{0}'".format(searchQuery)

	#animeList = data['media']
	#animeDict = dict()
	#retVal = ""

	#for anime in animeList:
	#	name = anime['title']['romaji']
	#	id = anime['id']
	#	animeDict[id] = name

	#if totalEntries <= 10:
	#	retVal = "{0} entries found:\n".format(totalEntries)
	#elif totalEntries > 10:
	#	retVal = "{0} entries found, showing the first 10:".format(totalEntries)

	#retVal += "```"
	#for key in animeDict:
	#	retVal += "'{0}': ID {1}\n".format(animeDict[key], key)
	#retVal += "```"

	#return retVal

def nextEpisodeAirsIn(animeId):
	apiResult = DAL.anilist_api.getNextAiringEpisode(animeId)
	if not __apiResultIsOk__(apiResult):
		return "Invalid JSON"
	data = apiResult['data']['Media']
	title = data['title']['romaji']
	nextAiringSeconds = int(data['nextAiringEpisode']['timeUntilAiring'])
	if nextAiringSeconds:
		nextAiringHours = int(nextAiringSeconds / 3600)
		nextAiringDays = int(nextAiringHours / 24)
		nextAiringHours = nextAiringHours % 24
		nextAiringString = ""
		
		if nextAiringDays > 0:
			return "{0}'s next episode airs in about {1} days and {2} hours.".format(title, nextAiringDays, nextAiringHours)
		else:
			return "{0}'s next episode airs in about {1} hours.".format(title, nextAiringHours)
	else:
		return "No next episode could be found for {0}.".format(title)

def searchAnime(searchQuery):
	apiResult = DAL.anilist_api.searchAnime(searchQuery)
	if not __apiResultIsOk__(apiResult):
		return "Error"
	data = apiResult['data']['Page']
	totalEntries = data['pageInfo']['total']
	if totalEntries == 0:
		return "No anime found for '{0}'".format(searchQuery)

	animeList = data['media']
	animeDict = dict()
	retVal = ""

	for anime in animeList:
		name = anime['title']['romaji']
		id = anime['id']
		# Add dict entry with id as key and name as value
		animeDict[id] = name

	if totalEntries <= 10:
		retVal = "{0} entries found:\n".format(totalEntries)
	elif totalEntries > 10:
		retVal = "{0} entries found, showing the first 10:".format(totalEntries)

	retVal += "```"
	for key in animeDict:
		retVal += "'{0}': ID {1}\n".format(animeDict[key], key)
	retVal += "```"

	return retVal

def subscribeToSeries(subscriber_id, subscriber_type, series_id):
	# Ask the DAL to do all the work
	DalResult = DAL.SQLite.subscribeToSeries(subscriber_id, subscriber_type, series_id)
	if DalResult:
		target = ""
		if subscriber_type.lower() == "user":
			target = "you"
		elif subscriber_type.lower() == "channel":
			target = "the channel"
		# Then send a message whether or not it succeeded
		return "Successfully subscribed {1} to {0}.".format(DalResult, target)
	else:
		return "Could not subscribe for some reason. It's possible the series has ended. If you're sure it hasn't, poke NearNile."

def updateNextAiring():
	numSeriesUpdated = DAL.SQLite.UpdateNextAiring()
	return numSeriesUpdated

def unsubscribeFromSeries(subscriber_id, subscriber_type, series_id):
	# Ask the DAL to do all the work
	DalResult = DAL.SQLite.unsubscribeFromSeries(subscriber_id, subscriber_type, series_id)
	if DalResult:
		target = ""
		if subscriber_type.lower() == "user":
			target = "you"
		elif subscriber_type.lower() == "channel":
			target = "the channel"
		# Then send a message whether or not it succeeded
		return "Successfully unsubscribed {0} from the thing.".format(target)
	else:
		return "Could not subscribe for some reason. Poke NearNile."

def listSubscriptions(subscriber_id, subscriber_type):
	DalResult = DAL.SQLite.listSubscriptions(subscriber_id, subscriber_type)
	if DalResult:
		return DalResult
	else:
		return

def checkSubscriptions():
	DalResult = DAL.SQLite.checkSubscriptions()
	return DalResult

def __apiResultIsOk__(apiResult):
	try:
		temp = apiResult['errors']
		return False
	except:
		return True