class subscriptionDbResult(object):
	"""description of class"""
	subscriber_id = ''
	series_name = ''
	nextepisode_number = 0
	series_totalepisodes = 0
	subscriber_type = ''

	def __init__(self, subscriber_id, series_name, nextepisode_number, series_totalepisodes, subscriber_type):
		"""Series DB result.
		
		Fields:
		subscriber_id: Subscriber ID (User/Channel).
		subscriber_type: Subscriber type [USER|CHANNEL].
		series_name: Well duh.
		nextepisode_number: Next episode number.
		series_totalepisodes: Total amount of episodes."""
		self.subscriber_id = subscriber_id
		self.subscriber_type = subscriber_type
		self.series_name = series_name
		self.nextepisode_number = nextepisode_number
		self.series_totalepisodes = series_totalepisodes