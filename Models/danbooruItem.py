import config
import math

# https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/
class danbooruItem(object):
	"""A Danbooru item, used to calculate how desirable an image is to show versus its competitors.
	Attributes:
		overallScore: Int giving numerical representation to desirability of the post. See code for calculation.
		partialUrl: Used internally, danbooru.donmai... etc
		url: Url to the post.
		desirableTags: Array of string noting which tags are desirable.
		undesirableTags: Array of string noting which tags are undesirable.
	"""
	
	overallScore = 0
	partialUrl = "https://danbooru.donmai.us/posts/"
	url = ""
	globalDesirableTags = config.DANBOORU_CONFIG['globalDesirableTags']
	globalUndesirableTags = config.DANBOORU_CONFIG['globalUndesirableTags']

	def __init__(self, id, score, favorites, deleted, tags, wantedTags = [], unwantedTags = []):
		"""Make a new danbooruItem object.
		
		Parameters:
			id: Danbooru's post ID.
			score: Danbooru's post score.
			favorites: Danbooru's post favorite count.
			deleted flag: Whether this post has been deleted or not.
			tags: Which tags this Danbooru post has.
			(un)wanted tags (optional): Specify wanted or unwanted tags. Useful in filtering out underwear for cute picture searches and such.
		
		Fills the following properties on itself:
			overallScore: Determines how 'valuable' a this instance is. Higher is better. Calculated by adding score and favorites together, then multiplies and divides based on tags.
			url: The URL of the post. danbooru.donmai.us/posts/id.
		"""

		# Tag list from string to array of strings, so we can check them
		tagList = tags.split()

		# Figure out of any of the desirable or undesirable tags are present
		# https://stackoverflow.com/questions/740287/how-to-check-if-one-of-the-following-items-is-in-a-list
		globalDesirableTagsPresent = [i for i in self.globalDesirableTags if i in tags]
		globalUndesirableTagsPresent = [i for i in self.globalUndesirableTags if i in tags]
		wantedTagsPresent = [i for i in wantedTags if i in tags]
		unwantedTagsPresent = [i for i in unwantedTags if i in tags]

		# Calculate score based on desirable and undesirable qualities
		# https://stackoverflow.com/questions/4172448/is-it-possible-to-break-a-long-line-to-multiple-lines-in-python
		self.overallScore = (score + favorites) \
			* (math.pow(2, (len(globalDesirableTagsPresent) + len(wantedTagsPresent)))) \
			* (math.pow(10, -1 * (len(globalUndesirableTagsPresent) + len(unwantedTagsPresent)))) \
			* int(deleted == False)		# deleted == 0 returns 1, deleted == 1 returns 0

		# Set the url based on the static bit and the id
		self.url = '{0}{1}'.format(self.partialUrl, id)