import os
from peewee import *

db = SqliteDatabase(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'mugibot.db')))

class alias(Model):
	"""Database model for aliases (keyword to tag)"""
	module = TextField()
	keyword = TextField()
	tag = TextField()

	class Meta:
		database = db
		constraints = [SQL('CONSTRAINT unique_aliases UNIQUE (module, keyword)')]