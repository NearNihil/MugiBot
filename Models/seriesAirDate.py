import os
from peewee import *

db = SqliteDatabase(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'mugibot.db')))

class seriesAirDate(Model):
	"""Series and their next air date."""
	id = PrimaryKeyField()
	series_id = IntegerField()							# 5680
	series_provider = TextField()						# ANILIST
	series_name = TextField()							# K-On!
	series_type = TextField()							# ANIME
	series_totalepisodes = IntegerField()				# 50
	nextepisode_airtime = DateTimeField(null=False)		# 2018/08/29 13:37
	nextepisode_number = IntegerField()					# 40

	class Meta:
		database = db
		constraints = [SQL('CONSTRAINT unique_series UNIQUE (series_id, series_provider, series_name, series_type, series_totalepisodes, nextepisode_airtime, nextepisode_number)')]