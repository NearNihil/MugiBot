import os
from peewee import *

db = SqliteDatabase(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'mugibot.db')))

class personalTagPreference(Model):
	"""Database model for personal tag preferences (both wanted and unwanted)"""
	userId = TextField()
	module = TextField()
	tag = TextField()
	wanted = BooleanField()

	class Meta:
		database = db
		constraints = [SQL('CONSTRAINT unique_aliases UNIQUE (userId, tag, module)')]