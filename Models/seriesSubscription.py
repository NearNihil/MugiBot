import os
from peewee import *
from Models import seriesAirDate

db = SqliteDatabase(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'mugibot.db')))

class seriesSubscription(Model):
	"""Database model for subscribing to series (anime or manga)"""
	subscriber_id = TextField()												# UserId or ChannelId
	subscriber_type = TextField()											# USER or CHANNEL
	seriesairdate_id = ForeignKeyField(seriesAirDate.seriesAirDate.id)		# 1
	
	class Meta:
		database = db
		constraints = [SQL('CONSTRAINT unique_subscription UNIQUE (subscriber_id, subscriber_type, seriesairdate_id)')]